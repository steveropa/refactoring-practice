package com.gildedrose;

class GildedRose {
    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item selected_item : items) {
            if(IsLegendary(selected_item)) {
                continue;
            }
            updateQuality(selected_item);
            updateSellin(selected_item);
        }
    }

    private static void updateSellin(Item selected_item) {
        if (!IsLegendary(selected_item)) {
            selected_item.sellIn = selected_item.sellIn - 1;
        }

        if (selected_item.sellIn < 0) {
            handleSellInDatePassed(selected_item);
        }
    }

    private static boolean IsLegendary(Item selected_item) {
        return selected_item.name.equals(SULFURAS);
    }

    private static void handleSellInDatePassed(Item selected_item) {
        if (!selected_item.name.equals(AGED_BRIE)) {
            if (!selected_item.name.equals(BACKSTAGE_PASSES)) {
                if (selected_item.quality > 0) {
                    if (!IsLegendary(selected_item)) {
                        selected_item.quality = selected_item.quality - 1;
                    }
                }
            } else {
                selected_item.quality = 0;
            }
        } else {
            if (selected_item.quality < 50) {
                selected_item.quality = selected_item.quality + 1;
            }
        }
    }

    private static void updateQuality(Item selected_item) {
        if (isANormalItem(selected_item)) {
            if (selected_item.quality > 0) {
                if (!IsLegendary(selected_item)) {
                    selected_item.quality = selected_item.quality - 1;
                }
            }
        } else {
            if (selected_item.quality < 50) {
                selected_item.quality = selected_item.quality + 1;

                if (selected_item.name.equals(BACKSTAGE_PASSES)) {
                    if (selected_item.sellIn < 11) {
                        if (selected_item.quality < 50) {
                            selected_item.quality = selected_item.quality + 1;
                        }
                    }

                    if (selected_item.sellIn < 6) {
                        if (selected_item.quality < 50) {
                            selected_item.quality = selected_item.quality + 1;
                        }
                    }
                }
            }
        }
    }

    private static boolean isANormalItem(Item selected_item) {
        return !selected_item.name.equals(AGED_BRIE)
                && !selected_item.name.equals(BACKSTAGE_PASSES);
    }
}