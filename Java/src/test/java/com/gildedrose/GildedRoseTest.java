package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {


    @Test
    void NormalItemsQualityDecreasesByOne(){
        Item[] items = new Item[] {new Item("Chocolate Bar", 10, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(19, app.items[0].quality);
    }

    @Test
    void NormalItemsQualityDecreasesByTwoAfterSellin(){
        Item[] items = new Item[] {new Item("Chocolate Bar", 0, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(18, app.items[0].quality);

    }

    @Test
    void ItemQualityIsNeverNegative(){

        Item[] items = new Item[] {new Item("Chocolate Bar", 0, 0)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(0, app.items[0].quality);

    }

    @Test
    void AgedBrieIncreasesInQuality(){
        Item[] items = new Item[] {new Item(GildedRose.AGED_BRIE, 10, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(21, app.items[0].quality);
    }

    @Test
    void LegendaryItemQualityNeverChanges(){
        Item[] items = new Item[] {new Item(GildedRose.SULFURAS, 10, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(20, app.items[0].quality);
    }

    @Test
    void LegendaryItemNeverHasToBeSold(){
        Item[] items =  new Item[] {new Item(GildedRose.SULFURAS, 10, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(10, app.items[0].sellIn);
    }

    @Test
    void
    IteratingOverMultipleItemsStillAdjustsQualities(){
        Item[] items = BuildTestArray();
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(19,items[0].quality);
        assertEquals(1,items[1].quality);
        assertEquals(4,items[2].sellIn);
        assertEquals(80,items[3].quality);
        assertEquals(80,items[4].quality);
        assertEquals(21,items[5].quality);
        assertEquals(47,items[6].quality);
        assertEquals(48,items[7].quality);
    }

    @Test
    void
    BackStagePassesWithinTenDaysIncreasesByTwo(){
        Item[] items = new Item[] {new Item(GildedRose.BACKSTAGE_PASSES, 10, 20)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(22,app.items[0].quality);

    }


    public Item[]
    BuildTestArray() {
        return new Item[]{
                new Item("+5 Dexterity Vest", 10, 20), //
                new Item("Aged Brie", 2, 0), //
                new Item("Elixir of the Mongoose", 5, 7), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 45),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 45),

        };
    }

}
